const slugify = require("slugify");

module.exports = function (eleventyConfig) {

	/**
	 * Return the index vector of a given content ID in a grid
	 */
	function findIndex(content, grid) {
		for (const row in grid) {
			for (const col in grid[row]) {
				if (grid[row][col] === content) {
					return [parseInt(row), parseInt(col)];
				}
			}
		}
	}

	/**
	 * Generate permalink from title using slugify
	 */
	function permalink(str) {
		return slugify(str, {
			lower: true,
			replacement: "-",
			remove: /[*+~·,.()'"`´%!?¿:@\/]/g,
		})
	}

	// Copy all assets folder
	eleventyConfig.addPassthroughCopy("src/assets/img");

	// Filters
	eleventyConfig.addFilter("slug", str => permalink(str));

	/**
	 * Return the cell link at the bottom of a given index
	 */
	function getBottomCell(currentIndex, grid, posts) {
		let nextIndex = [currentIndex[0] + 1, currentIndex[1]];
		// If current is at the bottom, return empty link
		if (nextIndex[0] >= grid.length) {
			nextIndex = null;
		}
		return getCell(nextIndex, 'bottom', grid, posts);
	}

	/**
	 * Return the cell link to the right of a given index
	 */
	function getRightCell(currentIndex, grid, posts) {
		let nextIndex = [currentIndex[0], currentIndex[1] + 1];
		// If current is at the right-most column, return empty link
		if (nextIndex[1] >= grid[0].length) {
			nextIndex = null;
		}
		return getCell(nextIndex, 'right', grid, posts);
	}

	/**
	 * Return the cell link at the top of a given index
	 */
	function getTopCell(currentIndex, grid, posts) {
		const nextIndex = (currentIndex[0] === 0)
			? null
			: [currentIndex[0] - 1, currentIndex[1]];
		return getCell(nextIndex, 'top', grid, posts);
	}

	/**
	 * Return the cell link to the left of a given index
	 */
	function getLeftCell(currentIndex, grid, posts) {
		const nextIndex = (currentIndex[1] === 0)
			? null
			: [currentIndex[0], currentIndex[1] - 1];
		return getCell(nextIndex,'left', grid, posts);
	}

	/**
	 * Get post data set
	 */
	function getCellData(index, grid, posts) {
		const [row, col] = index;
		const cellId = grid[row][col];

		// If empty cell, return null
		if (!cellId) {
			return null;
		}

		// Find cellId in the post collection
		for (const post of posts) {
			if (post.data.cellId === cellId) {
				return post.data;
			}
		}

		// cellId was not found in the posts collection
		return null;
	}

	/**
	 * Return the cell link given a cell index
	 */
	function getCell(nextIndex, position, grid, posts) {
		// If nextIndex is null, we are at the borders
		if (!nextIndex) {
			return '';
		}

		const data = getCellData(nextIndex, grid, posts);
		let nextCellUrl = '';
		let newtCellTitle = '';

		// If data is null, means it's a blank cell
		if (!data) {
			nextCellTitle = '';
			nextCellClass = 'blank';
			nextCellUrl = 'blank/cell-' + nextIndex.join('-') + '/';
		} else {
			nextCellTitle = data.title;
			nextCellClass = data.postClass;
			nextCellUrl = permalink(data.title);
		}

		return `
			<a class="nav-link ${nextCellClass} ${position}" href="/${nextCellUrl}">
				<div class="arrow"></div>
				<span class="cell-title">${nextCellTitle}</span>
			</a>`;
	}

	function getNavLink(type, index, grid, posts) {
		switch (type) {
			case 'right':
				return getRightCell(index, grid, posts);
			case 'left':
				return getLeftCell(index, grid, posts);
			case 'top':
				return getTopCell(index, grid, posts);
			case 'bottom':
				return getBottomCell(index, grid, posts);
			default:
				return getCell(index, '', grid, posts);
		}
	}

	// Shortcodes
	eleventyConfig.addShortcode("getNavLinkFromBlank", function(type, cell, grid, posts) {
		const currentIndex = cell.split('-').map(e => parseInt(e));
		return getNavLink(type, currentIndex, grid, posts);
	});

	eleventyConfig.addShortcode("getNavLink", function(type, cell, grid, posts) {
		const currentIndex = findIndex(cell, grid);
		return getNavLink(type, currentIndex, grid, posts);
	});

	// Directory names
	return {
		dir: {
			input: './src',
			data: '../_data',
			includes: '../_includes'
		}
	}
}
