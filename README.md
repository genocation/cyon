# CYON (Choose Your Own Navigation)

This project uses 11ty SSG (Static Site Generator) to create a site with a 2D navigation grid that
allows the user to have a different navigation experience. Such weird. Much experiment.

## Reference links
* Eleventy: https://www.11ty.dev/docs/
* Liquid: https://shopify.github.io/liquid/
* Example theme once: https://github.com/jalediazb/once
* Example theme yetty: https://github.com/ygoex/yetty


## Navigation

|---|----|----|----|----|----|
|   | a  | b  | c  | d  | e  |
|---|----|----|----|----|----|
| 1 | p1 | x  | x  | x  | q5 |
| 2 | q1 | p2 | p3 | x  | f5 |
| 3 | x  | x  | q2 | x  | p5 |
| 4 | x  | f2 | f1 | x  | q4 |
| 5 | x  | q3 | p4 | f4 | x  |
|---|----|----|----|----|----|


Layouts:
- post
- quote
- figure

They should be flexible, so this means we can do content files or a whole JSON file with the data
information:


Content files:
- PRO: easier to visualize
- PRO: no need to escape markdown stuff
- CON: must edit multiple files if one is changed

```njk
---
title:
info:
stuff:
---

Content in markdown.

## Title in markdown.

Stuff, paragraphs, blah.
```

Or a JSON file:
- PRO: only edit one file, centralized repository of data
- CON: difficult to visualize and edit information
- CON: need escaping mechanisms to add MD and HTML

```json
[
  {
    "title": "bla",
    "info": "bla",
    "stuff": "bla",
    "date": "bla",
    "content": "Content in markdown.\n\n##Title in markdown\n\nStuff, paragraphs, blah"
  }
]
```


Every page needs to navigate to other 4 pages.
There should be only one source of information, so that we don't need to edit stuff.
Maybe we can have a central file where we identify pages, like:

```json
[
  [ 'p1' , null, null, null, 'q5' ],
  [ 'q1' , 'p2', 'p3', null, 'f5' ],
  [ null , null, 'q2', null, 'p5' ],
  [ null , 'f2', 'f1', null, 'q4' ],
  [ null , 'q3', 'p4', 'f4', null ],
]
```

Each page should have an id, which is a string, and this ID can be used to get the url of the next,
previous, top and bottom pages, with a filter

{{ nextPage: 'p1' }}

```json
{}
```
