exports.data = {
	layout: 'blank',
	pagination: {
		data: 'grid',
		size: 1,
		before: function(data) {
			const cells = [];
			for (const row in data) {
				for (const col in data[row]) {
					if (data[row][col] == null) {
						cells.push(`${row}-${col}`);
					}
				}
			}
			return cells;
		}
	},
	permalink: function(ctx) {
		return `blank/cell-${ ctx.pagination.items[0] }/index.html`;
	}
}
