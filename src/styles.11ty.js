const path = require('path')
const sass = require('sass')

const inputFile = path.join(__dirname, '../_includes/scss/main.scss')

module.exports = class {
  data() {
    return {
      permalink: 'assets/css/styles.css',
      eleventyExcludeFromCollections: true
    }
  }

  async render() {
    const result = sass.compile(inputFile)
    return `${result.css}`
  }
}

